#!/usr/bin/env python3
from sys import argv
from subprocess import Popen
from os import environ
from pathlib import Path


def usage():
    print(f'Usage: {argv[0]} [steam|monado]')


if len(argv) <= 1:
    usage()
    exit(1)

target = argv[1].strip().lower()

HELP_ARGS = [
    'help', '-h', '--help',
]

STEAM = 'steam'
MONADO = 'monado'

RUNTIMES = [STEAM, MONADO]

VALID_ARGS = [
    *RUNTIMES, *HELP_ARGS
]

if target not in VALID_ARGS:
    print(f'Unrecognized argument `{target}`')
    exit(1)

if target in HELP_ARGS:
    usage()
    exit()

HOME = environ.get('HOME')
if HOME is None:
    print('Error: could not find HOME environment variable')
    exit(1)

CONF_DIR = Path(
    (
        environ.get('XDG_CONFIG_HOME') or
        (HOME+'/.config')
    )
)

OPENXR_DIR = CONF_DIR.joinpath('openxr/1')
OPENVR_DIR = CONF_DIR.joinpath('openvr')

if not OPENXR_DIR.is_dir():
    print(f'Error: OpenXR config dir (`{OPENXR_DIR}`) does not exist')
    exit(1)
if not OPENXR_DIR.is_dir():
    print(f'Error: OpenVR config dir (`{OPENVR_DIR}`) does not exist')
    exit(1)

OPENXR_STEAM = OPENXR_DIR.joinpath('active_runtime.json.steamvr').absolute()
OPENVR_STEAM = OPENVR_DIR.joinpath('openvrpaths.vrpath.steamvr').absolute()

OPENXR_FREE = OPENXR_DIR.joinpath('active_runtime.json.monado').absolute()
OPENVR_FREE = OPENVR_DIR.joinpath('openvrpaths.vrpath.opencomp').absolute()

OPENXR_TARGET = OPENXR_DIR.joinpath('active_runtime.json').absolute()
OPENVR_TARGET = OPENVR_DIR.joinpath('openvrpaths.vrpath').absolute()

if target == STEAM:
    if not OPENXR_STEAM.is_file():
        print(f'Error: OpenXR Steam config (`{OPENXR_STEAM}`) does not exist')
        exit(1)
    if not OPENVR_STEAM.is_file():
        print(f'Error: OpenVR Steam config (`{OPENVR_STEAM}`) does not exist')
        exit(1)
    print('Changing VR runtime to Steam')
    Popen([
        'ln', '-sf',
        str(OPENXR_STEAM),
        str(OPENXR_TARGET)
    ]).communicate()
    Popen([
        'ln', '-sf',
        str(OPENVR_STEAM),
        str(OPENVR_TARGET)
    ]).communicate()
    exit()

if target == MONADO:
    if not OPENXR_STEAM.is_file():
        print(f'Error: OpenXR Monado config (`{OPENXR_FREE}`) does not exist')
        exit(1)
    if not OPENVR_STEAM.is_file():
        print(f'Error: OpenVR Monado config (`{OPENVR_FREE}`) does not exist')
        exit(1)
    print('Changing VR runtime to Monado')
    Popen([
        'ln', '-sf',
        str(OPENXR_FREE),
        str(OPENXR_TARGET)
    ]).communicate()
    Popen([
        'ln', '-sf',
        str(OPENVR_FREE),
        str(OPENVR_TARGET)
    ]).communicate()
    exit()
